package br.com.csvservice.csvservice.controller;


import br.com.csvservice.csvservice.domain.model.Base;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

@RestController
@RequestMapping("/csv")
public class CsvController {

    @PostMapping
    public ResponseEntity uploadCSVFile(@RequestParam ("file")MultipartFile file, Model model ) throws IOException {

        byte filesize = (byte) 52428800;
        //validação
        if (file.isEmpty()){

        } else {

            //parsando o CSV para criar a lista de objetos

            try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))){

                //criando csv bean reader
                CsvToBean<Base> csvToBean = new CsvToBeanBuilder(reader)
                        .withType(Base.class)
                        .build();

                //convertendo o objeto CsvToBean para uma lista
                List<Base> base = csvToBean.parse();


                //Salvando dados em BD

                //salvando lista de base
                model.addAttribute("base", base);
                model.addAttribute("status", true);


            } catch (Exception e){
                model.addAttribute("mensagem", "Ocorreu um erro processando o arquivo CSV.");
                model.addAttribute("status", false);
            }

        }
        return ResponseEntity.ok(200);

    }



}
