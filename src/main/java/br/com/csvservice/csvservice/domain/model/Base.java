package br.com.csvservice.csvservice.domain.model;

import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Base {

    @CsvBindByName
    private long userId;

    @CsvBindByName
    private int serviceId;

    @CsvBindByName
    private String offerKey;

    @CsvBindByName
    private String createDate;

    @CsvBindByName
    private String digest;

    @CsvBindByName
    private String channel;
}
